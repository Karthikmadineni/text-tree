ROOT IN
MULTI-LINE FORM
+--- A in
|    multi-line
|    form
+--- B
|    +--- B1
|    +--- B2
|    |    `--- B21 in
|    |         multi-line form
|    `--- B3
|         +--- B31
|         `--- B32
`--- C in
     multi-line
     form
     +--- C1
     `--- C2 in multi-line
          form

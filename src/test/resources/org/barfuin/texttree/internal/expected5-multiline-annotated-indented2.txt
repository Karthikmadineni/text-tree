ROOT
## annotation
#+-- A
##      annotation
#+-- B
##   #+-- B1
##   #+-- B2
##   ##   #+-- B21
##   ##   ##      multi-line
##   ##   ##      annotation
##   ##   ``-- This is a long node text
##   ##        B22_xxxxxx
##   ##           multi-line
##   ##           annotation
##   ``-- B3
##        #+-- B31
##        ``-- B32
``-- C
     ## more multi-line
     ## annotation
     #+-- C1
     ``-- C2
             annotation

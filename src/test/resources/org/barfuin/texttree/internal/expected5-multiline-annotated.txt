ROOT IN
MULTI-LINE FORM
annotation
+--- A in
|    multi-line
|    form
|    annotation
+--- B
|    +--- B1
|    +--- B2
|    |    `--- B21 in
|    |         multi-line form
|    |         multi-line
|    |         annotation
|    `--- B3
|         +--- B31
|         `--- B32
`--- C in
     multi-line
     form
     more multi-line
     annotation
     +--- C1
     `--- C2 in multi-line
          form
          annotation
